package com.kalah.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kalah.model.GamePosition;
import com.kalah.service.GameService;

/**
 * 
 * @author Hemraj
 *
 */
@RestController
@RequestMapping("/game")
public class GameController {

	@Autowired
	private GameService gameService;
	
	private static GamePosition gameCurrentPosition = new GamePosition();
	
	@RequestMapping(value="/reset", method = RequestMethod.GET)
	public GamePosition save(){		
		gameCurrentPosition.reset();
		return gameCurrentPosition;
	}
	
	@RequestMapping(value="/move/{player}/{pitNo}", method = RequestMethod.GET)
	public GamePosition move(@PathVariable int player, @PathVariable int pitNo){
		gameCurrentPosition = gameService.moveStones(player, pitNo, gameCurrentPosition); 
		return gameCurrentPosition;
	}
}
