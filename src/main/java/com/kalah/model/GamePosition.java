package com.kalah.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
/**
 * Game position pojo which will be cached as static field in controller.
 * @author Hemraj
 *
 */
public class GamePosition {

	private Map<Integer, Integer> stonePositionMap;
	private int nextPlayer;
	private boolean isGameFinished;
	private String winnerMessage;
	
	public boolean isGameFinished() {
		return isGameFinished;
	}
	public Map<Integer, Integer> getStonePositionMap() {
		return stonePositionMap;
	}
	public void setStonePositionMap(Map<Integer, Integer> stonePositionMap) {
		this.stonePositionMap = stonePositionMap;
	}
	public void setGameFinished(boolean isGameFinished) {
		this.isGameFinished = isGameFinished;
	}	
	public int getNextPlayer() {
		return nextPlayer;
	}
	public void setNextPlayer(int nextPlayer) {
		this.nextPlayer = nextPlayer;
	}
	public String getWinnerMessage() {
		return winnerMessage;
	}
	public void setWinnerMessage(String winnerMessage) {
		this.winnerMessage = winnerMessage;
	}
	public void reset(){
		
		stonePositionMap = new HashMap<Integer, Integer>();
		
		stonePositionMap.put(0, 0); //Player-1's House
		stonePositionMap.put(1, 6); 
		stonePositionMap.put(2, 6);
		stonePositionMap.put(3, 6);
		stonePositionMap.put(4, 6);
		stonePositionMap.put(5, 6);
		stonePositionMap.put(6, 6);
		
		stonePositionMap.put(7, 0); //Player-2's House
		stonePositionMap.put(8, 6);
		stonePositionMap.put(9, 6);
		stonePositionMap.put(10, 6);
		stonePositionMap.put(11, 6);
		stonePositionMap.put(12, 6);
		stonePositionMap.put(13, 6);
		
		winnerMessage = null;
		isGameFinished = false;
		
		Random r = new Random();
		nextPlayer = r.nextInt(2 - 1) + 1;
	}
}
