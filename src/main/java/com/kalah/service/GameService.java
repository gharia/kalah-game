package com.kalah.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.kalah.model.GamePosition;

/**
 * 
 * @author Hemraj
 *
 */
@Service
public class GameService {

	/**
	 * 
	 * @param currentPlayer - Player 1 or 2 as int
	 * @param pitNo - pit that has been clicked
	 * @param gamePosition - current game position
	 * @return new game position as per move
	 */
	public GamePosition moveStones(int currentPlayer, int pitNo, GamePosition gamePosition){
		
		Map<Integer, Integer> stonePostionMap = gamePosition.getStonePositionMap(); 
		int noOfStones = stonePostionMap.get(pitNo);
		int currentPosition = pitNo;
		stonePostionMap.put(currentPosition, 0); //Setting stones at current position as 0 because all will be moved out.
		while(noOfStones > 0){
			if(currentPosition == 13){
				if(currentPlayer == 1){
					currentPosition = 0;
				}else{
					currentPosition = 1;
				}				
			}else if(currentPosition == 6){
				if(currentPlayer == 2){
					currentPosition = 7;
				}else{
					currentPosition = 8;
				}
			}else{
				currentPosition += 1; 
			}
			stonePostionMap.put(currentPosition, stonePostionMap.get(currentPosition) + 1);
			noOfStones--;
		}
		
		//Decide who's turn next will be
		if(currentPosition == 0 && currentPlayer == 1){
			gamePosition.setNextPlayer(1);
		}else if(currentPosition == 7 && currentPlayer == 2){
			gamePosition.setNextPlayer(2);
		}else{ //If normal game flow then just change the turn of player
			if(currentPlayer == 1){
				gamePosition.setNextPlayer(2);
			}else{
				gamePosition.setNextPlayer(1);
			}
		}
		
		//Check if last stone landed in current player's empty house
		if(stonePostionMap.get(currentPosition) == 1){
			int housePosition = -1; //Setting default house position for putting captured stone to -1;
			if(currentPlayer == 2 && currentPosition > 7 ){ //Means captured stone is to be put in Player-2's house
				housePosition = 7;
			}else if(currentPlayer == 1 && currentPosition > 0 && currentPosition < 7){ //Means captured stone is to be put in Player-1's house
				housePosition = 0;
			}
			if(housePosition >= 0){ //If Player-1's or Player-2's house qualify according to above condition
				int totalStoneInKalah = stonePostionMap.get(housePosition) + stonePostionMap.get(currentPosition) + stonePostionMap.get(stonePostionMap.size() - currentPosition);
				stonePostionMap.put(housePosition, totalStoneInKalah);
				stonePostionMap.put(currentPosition,0);
				stonePostionMap.put(stonePostionMap.size() - currentPosition, 0);
			}			
		}
		
		gamePosition.setStonePositionMap(stonePostionMap);
		gamePosition = gameOverCheck(gamePosition); //Check if the game is over
		
		return gamePosition;
	}
	
	/**
	 * Checks whether the game is over or not. game is over if any of the player runs out of stones
	 * @param gamePosition current game position after move
	 * @return Modified Game Position with game over flag and result message if over
	 */
	public GamePosition gameOverCheck(GamePosition gamePosition){
		boolean player1Empty = true, player2Empty = true;
		int index = 1;
		while(index <= 13){
			if(index == 7){
				if(player1Empty == true){
					player2Empty = false;
					break;
				}
				index++;
				continue;
			}
			if(gamePosition.getStonePositionMap().get(index) > 0){
				if(index < 7){			
					index = 8;
					player1Empty = false;
				}else{
					player2Empty = false;
					break;
				}
			}
			index++;
		}
		if(player1Empty){ //capture all from Player-2's pits to his house
			gamePosition = captureAllToHouse(gamePosition, 8, 13, 7);
			gamePosition.setGameFinished(true);			
		}else if(player2Empty){ //capture all from Player-1's pits to his house
			gamePosition = captureAllToHouse(gamePosition, 1, 6, 0);
			gamePosition.setGameFinished(true);
		}
		
		//Add Result message if game is over
		if(player1Empty || player2Empty){
			String winnerMsg = "";
			if(gamePosition.getStonePositionMap().get(0) == gamePosition.getStonePositionMap().get(7)){
				winnerMsg = "Game Over! There is Tie!";
			}else if(gamePosition.getStonePositionMap().get(0) > gamePosition.getStonePositionMap().get(7)){
				winnerMsg = "Game Over! Player 1 wins!";
			}else{
				winnerMsg = "Game Over! Player 2 wins!";
			}
			gamePosition.setWinnerMessage(winnerMsg);
		}
				
		return gamePosition;
		
	}
	
	/**
	 * If the game is over, then next capture all the stones from opponent's pits to his/her house
	 * @param gamePosition current game position
	 * @param startIndex index of pit from where to start collecting stones
	 * @param endIndex index of last pit of player
	 * @param houseIndex house index of player's house
	 * @return Modified game position after all the stones collected from player pits and added to his house
	 */
	public GamePosition captureAllToHouse(GamePosition gamePosition, int startIndex, int endIndex, int houseIndex){
		
		Map<Integer, Integer> stonePostionMap = gamePosition.getStonePositionMap();
		
		for(int i = startIndex; i <= endIndex; i++){
			stonePostionMap.put(houseIndex, stonePostionMap.get(houseIndex) + stonePostionMap.get(i));
		}
		
		gamePosition.setStonePositionMap(stonePostionMap);
		
		return gamePosition;
	}
}
