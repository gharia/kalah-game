//= wrapped
//= require /core/kalah.core

(function() {
	
	var game = angular.module("game", [        
	    "kalah.core"
	]);
	
	var gameService = function (DomainServiceFactory) {

        return DomainServiceFactory('/game/:action/:player/:pitNo',{action:'@action', player:'@player', pitNo: '@pitNo'},
            {"reset": {method: "GET"},
            "move": {method: "GET"}}            
        );

    };
    
    game.factory("gameService",gameService);
    
	var GameController = function(gameService, $scope){
		var self = this;	
		
		$scope.gamePosition = {};
	    
		$scope.resetGame = function(){
			gameService.reset({action:'reset'},function(data){
				$scope.gamePosition = data;
	        });
		}
		
		$scope.resetGame();
		
		$scope.moveStones = function(player, pitNo){
			if($scope.gamePosition.nextPlayer != player){
				alert("This Player does not have turn!");
			}else{
				gameService.move({action:'move', player:player, pitNo: pitNo},function(data){
					$scope.gamePosition = data;
		        });
			}
		};
	};
	
	game.controller("GameController", ['gameService','$scope', GameController]);
}());
