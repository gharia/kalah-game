//= wrapped
//= require /jquery/jquery.min
//= require /materialize/js/materialize.min
//= require /angular/angular.min
//= require /angular/angular-resource.min
//= require_self
//= require_tree services

angular.module("kalah.core", ['ngResource']);